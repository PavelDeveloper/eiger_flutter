
class Constants {
  static const String BASE_URL = "http://192.168.1.230:8125/api/";
 // static const String BASE_URL = "https://eiger-api.gefira.ch/api/";

  static const String BASE_URL_FOR_SHARE = "https://eiger.gefira.ch/";
  static const String BASE_PART_PUBLIC = "public-event/";
  static const String BASE_PART_PRIVATE = "private-event/";
  static const String PRIVATE_KEY = "private_key=";
}
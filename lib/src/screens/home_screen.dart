import 'package:eiger/src/common/router.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:eiger/src/controller/home_controller.dart' show Con;
import 'package:eiger/src/screens/event_screen.dart';
import 'package:eiger/src/screens/login_screen.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:qrcode_reader/qrcode_reader.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:eiger/src/data/pref_manager.dart';

class HomeScreen extends StatefulWidget {
  static const String ROUTE = "/";

  @protected
  @override
  createState() => HomeView();
}

class HomeView extends State<HomeScreen> {
  final Con _con = new Con();
  TextEditingController nameControl = new TextEditingController();

  @protected
  @override
  void initState() {
    super.initState();
    PrefManager.getToken().then((token) {
      if (token != null && token.isNotEmpty) {
        Router.replace(context, ProfileScreen());
      }
    });
    requestPermission();
  }

  @protected
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

          /// The View need not know nor care what the title is. The Controller does.
          title: Text(_con.title),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.input),
              onPressed: () {
                Router.navigateTo(context, LoginScreen());
              },
            )
          ]),
      body: ListView(
        children: [
          Image.asset('images/create_event_image.png'),
          new Container(
              margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              child: TextFormField(
                  decoration: InputDecoration(labelText: 'Create Event'),
                  controller: nameControl,
                  keyboardType: TextInputType.text,
                  maxLength: 32)),
          new Container(
              margin: const EdgeInsets.all(20.0),
              child: RaisedButton(
                padding: const EdgeInsets.all(12.0),
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  createEvent();
                },
                child: new Text("Create Event"),
              )),
          new Container(
              margin: const EdgeInsets.only(left: 40.0, right: 40.0),
              child: RaisedButton(
                padding: const EdgeInsets.all(12.0),
                textColor: Colors.black,
                color: Colors.white,
                onPressed: () {
                  scanQrCode();
                },
                child: new Text("Scan QR code"),
              )),
        ],
      ),
    );
  }

  void createEvent() {
    if (nameControl.text.isNotEmpty) {
      setState(() {
        _con.createEvent(nameControl.text).then((resp) {
          print("CREATE EVENT ${resp.data.title}");
          if (resp.data != null) {
            Router.navigateTo(context,  EventScreen(data: resp.data));
          }
        });
      });
    }
  }

  void scanQrCode() {
    Future<String> futureString = new QRCodeReader()
        .setAutoFocusIntervalInMs(200) // default 5000
        .setForceAutoFocus(true) // default false
        .setTorchEnabled(true) // default false
        .setHandlePermissions(true) // default true
        .setExecuteAfterPermissionGranted(true) // default true
        .scan();

    futureString.then((result) {
      print("QR RESULT $result");
      final slug = result.substring(result.lastIndexOf("/") + 1, result.length);
      print("SLUG $slug");

      _con.getPublicEvent(slug).then((response) {
        if (response.data != null) {
          Router.navigateTo(context, EventScreen(data: response.data));
        }
      });
    });
  }

  requestPermission() async {
    await PermissionHandler()
        .requestPermissions([PermissionGroup.camera, PermissionGroup.storage]);
  }
}

import 'dart:io';

import 'package:eiger/constants.dart';
import 'package:eiger/src/controller/event_controller.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:eiger/src/network/model/my_events.dart';
import 'package:eiger/src/screens/view/media_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_multiple_image_picker/flutter_multiple_image_picker.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:multiple_image_picker/multiple_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:share/share.dart';

class EventScreen extends StatefulWidget {
  static const String ROUTE = "/event";
  MyEvents data;

  String token;
  EventScreen({Key key, @required this.data}) {
      getToken();
  }

  @override
  State<StatefulWidget> createState() {
    return EventView(data, token,EventController());
  }

  void getToken() async {
    await PrefManager.getToken().then((token) {
        this.token = token;
    });
  }
}

class EventView extends StateMVC {
  String token;

  //final Con _con = new Con();
  EventView(this.data, this.token, EventController con) : super(con);

  EventController _con = EventController.con;
  TextEditingController nameControl = new TextEditingController();
  MyEvents data;
  var crossAxisCount = 2;

  @protected
  @override
  Widget build(BuildContext context) {

    _con.initData(data);
    print("BUILD EVENT VIEW ${_con.data.title}");
    return Scaffold(
      appBar: AppBar(

          /// The View need not know nor care what the title is. The Controller does.
          title: Text("Event"),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                _con.deleteEvent();
              },
            )
          ]),
      bottomNavigationBar: makeBottom(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          shareEvent();
        },
        child: Icon(Icons.share),
      ),
      body: ListView(shrinkWrap: true, children: [
        new Container(
            color: Colors.grey[300],
            padding: EdgeInsets.all(15),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  _con.data.title ?? 'Title empty',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                Text(
                  _con.getDescription(),
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                ),
              ],
            )),
        Container(
            color: Colors.grey[200],
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.view_module, color: Colors.black),
                      onPressed: () {
                        setState(() {
                          crossAxisCount = 2;
                        });
                      },
                    ),
                    IconButton(
                        icon: Icon(Icons.view_stream, color: Colors.black),
                        onPressed: () {
                          setState(() {
                            crossAxisCount = 1;
                          });
                        })
                  ]),
                  Container(
                      alignment: Alignment.centerRight,
                      margin: const EdgeInsets.only(right: 15.0),
                      child: Text(
                        "Total files ${_con.getMedia().length}",
                        textAlign: TextAlign.end,
                        overflow: TextOverflow.ellipsis,
                      ))
                ])),
        new StaggeredGridView.countBuilder(
          crossAxisCount: crossAxisCount,
          itemCount: _con.getMedia().length,
          shrinkWrap: true,
          primary: false,
          itemBuilder: (BuildContext context, int index) => new MediaItem(
              _con.getMedia()[index],
              _con.getMedia(),
              data.isPrivate,
              token ?? ""),
          staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        )
        // Center(child: MediaList(_con.getMedia())),
      ]),
    );
  }

  Widget makeBottom(context) {
    return Container(
      height: 55.0,
      child: BottomAppBar(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.favorite_border, color: Colors.black),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.camera, color: Colors.black),
              onPressed: () {
                ImagePicker.pickImage(source: ImageSource.camera).then((file) {
                  uploadFile(file, file);
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.file_upload, color: Colors.black),
              onPressed: () async {
                await FlutterMultipleImagePicker.pickMultiImages(10, false)
                    .then((list) {
                  print("FILE : ${new File(list[0].toString()).path}");
                  for (var file in list) {
                    uploadFile(
                        new File(file.toString()), new File(file.toString()));
                  }
                });
              },
            )
          ],
        ),
      ),
    );
  }

  void uploadFile(File file, File crFile) {
    setState(() {
      _con.uploadPhoto(_con.data.slug, file, crFile);
    });
  }

  void shareEvent() {
    final shareUrl =
        Constants.BASE_URL_FOR_SHARE + Constants.BASE_PART_PUBLIC + data.slug;
    final RenderBox box = context.findRenderObject();
    Share.share(shareUrl,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }


}

import 'package:flutter/material.dart';

class CreateEventDialog extends StatefulWidget {
  CreateEventDialog(
      {Key key, this.titleController, this.descrController, this.privateEvent})
      : super(key: key);

  TextEditingController titleController;
  TextEditingController descrController;
  bool privateEvent;

  @override
  _DialogState createState() => new _DialogState(
      this.titleController, this.descrController, this.privateEvent);
}

class _DialogState extends State<CreateEventDialog> {
  bool isPrivate;
  TextEditingController titleController;
  TextEditingController descrController;

  _DialogState(this.titleController, this.descrController, this.isPrivate);

  void _onChanged(bool value) => setState(() => isPrivate = value);
  @override
  Widget build(BuildContext context) {
    return ListView(
          shrinkWrap: true,
          children: <Widget>[
            TextFormField(
                decoration: InputDecoration(labelText: 'Title'),
                controller: titleController),
            TextFormField(
                decoration: InputDecoration(labelText: 'Description'),
                controller: descrController),
            Row(
              children: <Widget>[
                Switch(
                    value: isPrivate,
                    activeColor: Colors.green,
                    inactiveThumbColor: Colors.red,
                    onChanged: _onChanged),
                Text(getSwitchState(isPrivate))
              ],
            )
          ],
        );
  }

  String getSwitchState(bool isActive) {
    if (isActive) {
      return "Private Event";
    } else {
      return "Public Event";
    }
  }
}

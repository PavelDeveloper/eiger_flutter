import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:eiger/src/common/router.dart';
import 'package:eiger/src/controller/gallery_controller.dart';
import 'package:eiger/src/network/model/media.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class GalleryScreen extends StatefulWidget {
  static const String ROUTE = "/gallery";
  List<Media> media;

  GalleryScreen({Key key, this.media});

  @override
  State<StatefulWidget> createState() {
    return GalleryView(GalleryController(), media);
  }
}

class GalleryView extends StateMVC {
  List<Media> media;

  //final Con _con = new Con();
  GalleryView(GalleryController con, this.media) : super(con);
  GalleryController _con = GalleryController.con;

  @protected
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /*  appBar: AppBar(
        title: Text("Login"),
      ),*/
        body: new CarouselSlider(
            items: media.map((i) {
              return new Builder(
                builder: (BuildContext context) {
                  return new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 50.0),
                      decoration: new BoxDecoration(color: Colors.transparent),
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Card(
                                child: new CachedNetworkImage(
                              imageUrl: i.url,
                              placeholder: new CircularProgressIndicator(),
                              errorWidget:
                                  new Icon(Icons.photo_camera, size: 60),
                            ))
                          ]));
                },
              );
            }).toList(),
            height: 700.0,
            autoPlay: true));
  }

  void navigateToProfileIfNeed() {
    print("navigateToProfile token ${_con.authToken}");
    if (_con.authToken.isNotEmpty) {
      Router.replace(context, ProfileScreen());
    }
  }
}

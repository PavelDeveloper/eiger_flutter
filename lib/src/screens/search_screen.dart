import 'package:eiger/src/controller/search_controller.dart';
import 'package:eiger/src/network/model/user.dart';
import 'package:eiger/src/screens/view/event_click.dart';
import 'package:eiger/src/screens/view/users_list.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SearchScreen extends StatefulWidget {
  static const String ROUTE = "/search";

  SearchScreen({Key key});

  @override
  State<StatefulWidget> createState() {
    return SearchView(SearchController());
  }
}

class SearchView extends StateMVC implements EventClick {
  //final Con _con = new Con();
  SearchView(SearchController con) : super(con);
  SearchController _con = SearchController.con;
  GlobalKey<FormState> _key = new GlobalKey();
  var _searchEdit = new TextEditingController();

  bool _isSearch = true;
  String _searchText = "";

  @protected
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Search"),
        ),
        body: DefaultTabController(
            length: 3,
            initialIndex: 0,
            child: Column(
              children: [
                Container(
                    color: Colors.blue[300],
                    child: TabBar(
                        labelColor: Colors.white,
                        unselectedLabelColor: Colors.grey[300],
                        indicatorColor: Colors.white,
                        tabs: [
                          Tab(text: 'My'.toUpperCase()),
                          Tab(text: 'Find'.toUpperCase()),
                          Tab(text: 'Favorite'.toUpperCase())
                        ])),
                Flexible(
                    child: TabBarView(
                  children: [
                    new UsersList(_con.friends, this, 0),
                    getFind(),
                    getRequests()
                  ],
                ))
              ],
            )));
  }

  Widget getFind() {
    return new ListView(
      children: <Widget>[searchBox(), new UsersList(_con.search, this, 1)],
    );
  }

  Widget searchBox() {
    _searchEdit.addListener(() {
      if (_searchEdit.text.isEmpty) {
        setState(() {
          _isSearch = true;
          _searchText = '';
        });
      } else {
        _isSearch = false;
        _searchText = _searchEdit.text;

        _con.getUsers(_searchEdit.text);
      }
    });

    return new Container(
      margin: EdgeInsets.all(6),
      child: new TextFormField(
        controller: _searchEdit,
        decoration: InputDecoration(labelText: 'Name or Email:'),
        textAlign: TextAlign.start,
      ),
    );
  }

  Widget getRequests() {
    return new ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(margin: EdgeInsets.all(6), child: Text("My Requests:")),
        new UsersList(_con.my, this, 2),
        Container(margin: EdgeInsets.all(6), child: Text("Requests to Me:")),
        new UsersList(_con.toMe, this, 3),
      ],
    );
  }

  @override
  void onAcceptClick(User user, int typeEvent) {
    if(typeEvent == 1) {
      _con.addToFriends(user);
    } else {
      _con.acceptFriend(user);
    }
  }

  @override
  void onDeleteClick(User user, int typeEvent) {
    switch (typeEvent) {
      case 0:
        _con.deleteFriend(user);
        return;
      case 1:
        return;
      case 2:
        _con.deleteFriendship(user, typeEvent);
        return;
      case 3:
        _con.deleteFriendship(user, typeEvent);
        return;
    }
    print("onDeleteClick");
  }
}

import 'package:eiger/src/common/router.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/common/utils.dart';
import 'package:eiger/src/controller/register_controller.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class RegisterScreen extends StatefulWidget {
  static const String ROUTE = "/register";

  RegisterScreen({Key key});

  @override
  State<StatefulWidget> createState() {
    return RegisterView(RegisterController());
  }
}

class RegisterView extends StateMVC {
  //final Con _con = new Con();
  RegisterView(RegisterController con) : super(con);
  RegisterController _con = RegisterController.con;

  TextEditingController loginController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController passwordConfController = new TextEditingController();
  String _email;
  String _name;
  String _pass;
  String _passConf;
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;

  @protected
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: ListView(children: [
        new Container(
        margin: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: new Form(
          key: _key,
          autovalidate: _validate,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Email'),
                controller: loginController,
                keyboardType: TextInputType.emailAddress,
                validator: Utils.validateEmail,
                onSaved: (String val) {
                  _email = val;
                },
              ), TextFormField(
                decoration: InputDecoration(labelText: 'Name'),
                controller: nameController,
                keyboardType: TextInputType.text,
                onSaved: (String val) {
                  _name = val;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Password'),
                obscureText: true,
                controller: passwordController,
                onSaved: (String val) {
                  _pass = val;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Confirm Password'),
                obscureText: true,
                controller: passwordConfController,
                onSaved: (String val) {
                  _passConf = val;
                },
              ),
              new Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  child: RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.blue,
                    onPressed: () {
                      doLogin();
                    },
                    child: new Text("Register"),
                  ))
            ],
          ),
        ),
      )
      ])
    );
  }

  void doLogin() {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      print("Email: ${_email.toString()} Name: ${_name.toString()}  Pass: ${_pass.toString()} PassConf: ${_passConf.toString()}");
      _con.register(_email, _name, _pass, _passConf);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  void navigateToProfileIfNeed() {
    print("navigateToProfile token ${_con.authToken}");
    if (_con.authToken.isNotEmpty) {
      Router.replace(context, ProfileScreen());
    }
  }
}

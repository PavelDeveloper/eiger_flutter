import 'package:eiger/src/common/router.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:eiger/src/screens/event_screen.dart';
import 'package:eiger/src/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/controller/profile_controller.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:eiger/src/screens/view/event_list.dart';
import 'package:eiger/src/screens/dialog/create_event_dialog.dart';
import 'package:eiger/src/screens/search_screen.dart';

class ProfileScreen extends StatefulWidget {
  static const String ROUTE = "/profile";

  ProfileScreen({Key key});

  @override
  State<StatefulWidget> createState() {
    return ProfileView(ProfileController());
  }
}

class ProfileView extends StateMVC {
  //final Con _con = new Con();
  ProfileView(ProfileController con) : super(con);
  ProfileController _con = ProfileController.con;

  @protected
  @override
  Widget build(BuildContext context) {
    print("Is loading ${_con.isLoading}");
    if (_con.isLoading) {
      _con.getProfile();
      return new Center(
        child: new CircularProgressIndicator(),
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            title: Text("Profile"),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SearchScreen()),
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  PrefManager.saveToken("");
                  Router.replaceAllScreens(context, HomeScreen.ROUTE);
                },
              )
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialogCreateEvent();
            },
            child: Icon(Icons.add, color: Colors.white),
          ),
          body: Container(
              child: ListView(
            children: <Widget>[
              Container(
                color: Colors.grey[300],
                padding: EdgeInsets.only(top: 10, left: 10, bottom: 10),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        new CachedNetworkImage(
                          imageUrl: _con.getAvatar(),
                          placeholder: new CircularProgressIndicator(),
                          errorWidget: new Icon(Icons.people, size: 50),
                        ),
                      ],
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(left: 10, bottom: 30),
                        child: Text(
                          "Name:",
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        )),
                    Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(left: 5, bottom: 30),
                        child: Text(_con.getName() ?? '',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                              fontSize: 15,
                            )))
                  ],
                ),
              ),
              DefaultTabController(
                  length: 3,
                  initialIndex: 0,
                  child: Column(
                    children: [
                      Container(
                          color: Colors.blue[300],
                          child: TabBar(
                              labelColor: Colors.white,
                              unselectedLabelColor: Colors.grey[300],
                              indicatorColor: Colors.white,
                              tabs: [
                                Tab(
                                    text: 'My',
                                    icon: new Icon(Icons.calendar_today)),
                                Tab(
                                    text: 'Shared',
                                    icon: new Icon(Icons.calendar_view_day)),
                                Tab(
                                    text: 'Favorite',
                                    icon: new Icon(Icons.favorite_border))
                              ])),
                      Container(
                          height: 410.0,
                          child: TabBarView(
                            children: [
                              new EventList(_con?.eventData?.myEvents ?? new List()),
                              new EventList(_con?.eventData?.friendsEvents ?? new List()),
                              new EventList(_con?.eventData?.favoriteEvents ?? new List())
                            ],
                          ))
                    ],
                  ))
            ],
          )));
    }
  }

  void showDialogCreateEvent() {
    // flutter defined function
    TextEditingController titleController = new TextEditingController();
    TextEditingController descrController = new TextEditingController();
    bool isPrivate = true;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Create Event"),
          content: new CreateEventDialog(
              titleController: titleController,
              descrController: descrController,
              privateEvent: isPrivate),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Create"),
              onPressed: () {
                _con.createEvent(
                    titleController.text, descrController.text, isPrivate);
              },
            )
          ],
        );
      },
    );
  }
}


import 'package:eiger/src/screens/view/event_click.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/network/model/user.dart';
import 'package:eiger/src/screens/view/user_item.dart';

class UsersList extends StatelessWidget {

  List<User> list;
  EventClick eventClick;
  int typeEvent;

  UsersList(this.list, this.eventClick, this.typeEvent);


  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }

   _buildList(context) {
     if(list == null) {
       list = new List();
     }
    return ListView.builder(
      itemCount: list.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, int) {
        return UserItem(list[int], eventClick, typeEvent);
      },
    );
  }
}
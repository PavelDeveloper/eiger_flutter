
import 'package:eiger/src/network/model/user.dart';

abstract class EventClick {
  void onDeleteClick(User user, int typeEvent);
  void onAcceptClick(User user, int typeEvent);
}

import 'package:flutter/material.dart';
import 'package:eiger/src/network/model/my_events.dart';
import 'package:eiger/src/screens/view/event_item.dart';

class EventList extends StatelessWidget {

  List<MyEvents> list;

  EventList(this.list);


  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }

   _buildList(context) {
     if(list == null) {
       list = new List();
     }
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (context, int) {
        return EventItem(list[int]);
      },
    );
  }
}
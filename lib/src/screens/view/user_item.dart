import 'package:eiger/src/screens/view/event_click.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/network/model/user.dart';
import 'package:eiger/src/screens/view/custom_circle_avatar.dart';

class UserItem extends StatelessWidget {
  User user;
  EventClick eventClick;
  int typeEvent;

  UserItem(this.user, this.eventClick, this.typeEvent);

  @override
  Widget build(BuildContext context) {
    return _buildItem(context);
  }

  _buildItem(context) {
    return GestureDetector(
        child: Card(
            color: Colors.white,
            child: Container(
                padding: EdgeInsets.all(12),
                child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: <Widget>[
                          getAvatar(),
                          Container(
                              margin: EdgeInsets.all(5),
                              child: Text(user.name ?? '',
                                  textAlign: TextAlign.left)),
                        ],
                      ),
                      Row(
                        children: getButtons()
                            .map((c) => Container(
                                  padding: EdgeInsets.only(right: 3),
                                  child: c,
                                ))
                            .toList(),
                      )
                    ]))),
        onTap: () {});
  }

  List<Widget> getButtons() {
    List<Widget> list = new List<Widget>();
    switch(typeEvent) {
      case 0:
        list.add(IconButton(icon: Icon(Icons.close), onPressed: () {
          eventClick.onDeleteClick(user, typeEvent);
        }));
        return list;
      case 1:
        list.add(IconButton(icon: Icon(Icons.group_add), onPressed: () {
          eventClick.onAcceptClick(user, typeEvent);
        }));
        return list;
      case 2:
        list.add(IconButton(icon: Icon(Icons.close), onPressed: () {
          eventClick.onDeleteClick(user, typeEvent);
        }));
        return list;
      case 3:
        list.add(IconButton(icon: Icon(Icons.check), onPressed: () {
          eventClick.onAcceptClick(user, typeEvent);
        }));
        list.add(IconButton(icon: Icon(Icons.close), onPressed: () {
          eventClick.onDeleteClick(user, typeEvent);
        }));
        return list;
    }

    return list;
  }

  Widget getAvatar() {
    return new Container(
        height: 40.0,
        width: 40.0,
        child: new CustomCircleAvatar(
          myImage: new NetworkImage(user.avatarUrl ?? ''),
          initials: user.name[0].toUpperCase(),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:eiger/src/network/model/my_events.dart';
import 'package:eiger/src/screens/event_screen.dart';

class EventItem extends StatelessWidget {
  MyEvents event;

  EventItem(this.event);

  @override
  Widget build(BuildContext context) {
    return _buildItem(context);
  }

  _buildItem(context) {
    return GestureDetector(
        child: Card(
            color: Colors.white,
            child: Container(
                padding: EdgeInsets.all(12),
                child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: <Widget>[
                          getLock(),
                          Container(
                              margin: EdgeInsets.all(5),
                              child:
                                  Text(event.title ?? '', textAlign: TextAlign.left)),
                        ],
                      ),
                      Row(
                        children: getMediaRow()
                            .map((c) => Container(
                                  padding: EdgeInsets.only(right: 3),
                                  child: c,
                                ))
                            .toList(),
                      )
                    ]))),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EventScreen(data: event)),
          );
        });
  }

  Widget getLock() {
    if (event.isPrivate) {
      return Icon(Icons.lock, size: 18.0, color: Colors.black);
    } else {
      return Icon(Icons.lock_open, size: 18.0, color: Colors.black);
    }
  }

  List<Widget> getMediaRow() {
    List<Widget> list = new List<Widget>();
    list.add(Text(event.users.length.toString()));
    list.add(Icon(Icons.people));
    list.add(Text(event.media.length.toString()));
    list.add(Icon(Icons.image));
    return list;
  }
}

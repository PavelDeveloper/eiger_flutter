import 'package:cached_network_image/cached_network_image.dart';
import 'package:eiger/src/common/router.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:eiger/src/network/model/media.dart';
import 'package:eiger/src/screens/gallery_screen.dart';
import 'package:flutter/material.dart';

class MediaItem extends StatelessWidget {
  Media media;
  List<Media> list;
  bool isPrivate;
  String token;

  MediaItem(this.media, this.list, this.isPrivate, this.token) ;

  @override
  Widget build(BuildContext context) {
    return _buildItem(context);
  }

  _buildItem(context) {
    return GestureDetector(
        child: Card(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              new CachedNetworkImage(
                imageUrl: getUrl(),
                placeholder: new CircularProgressIndicator(),
                errorWidget: new Icon(Icons.photo_camera, size: 60),
              ),
              new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 8),
                        width: 65,
                        child: Text(
                          media.createdBy.name,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                              fontWeight: FontWeight.normal, fontSize: 14),
                        )),
                    IconButton(
                      alignment: Alignment.centerRight,
                      icon: Icon(Icons.cloud_download,
                          size: 18.0, color: Colors.black),
                      onPressed: () {},
                    ),
                    IconButton(
                      alignment: Alignment.centerRight,
                      icon: Icon(Icons.delete, size: 18.0, color: Colors.black),
                      onPressed: () {},
                    )
                  ])
            ],
          ),
        ),
        onTap: () {
          Router.navigateTo(context, GalleryScreen(media: list,));
        });
  }

  String getUrl() {
    if (isPrivate) {
      print("URL ${media.url + "?" + "token=" + token}");
      return media.url + "?" + "token=" + token;
    } else {
      print("URL ${media.url}");
      return media.url;
    }
  }
}

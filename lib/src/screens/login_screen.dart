import 'package:eiger/src/common/router.dart';
import 'package:eiger/src/common/utils.dart';
import 'package:eiger/src/controller/login_controller.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:eiger/src/screens/register_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class LoginScreen extends StatefulWidget {
  static const String ROUTE = "/login";

  LoginScreen({Key key});

  @override
  State<StatefulWidget> createState() {
    return LoginView(LoginController());
  }
}

class LoginView extends StateMVC {
  //final Con _con = new Con();
  LoginView(LoginController con) : super(con);
  LoginController _con = LoginController.con;

  TextEditingController loginController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  String _email;
  String _pass;
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;

  @override
  void initState() {
    super.initState();
    setState((){
    if (_con.errorMessage.isNotEmpty) {
      _validate = true;
    }
    });
  }

  @protected
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: ListView(children: [
          new Container(
            margin: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: new Form(
              key: _key,
              autovalidate: _validate,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Email'),
                    controller: loginController,
                    keyboardType: TextInputType.emailAddress,
                    validator: Utils.validateEmail,
                    onSaved: (String val) {
                      _email = val;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Password'),
                    obscureText: true,
                    controller: passwordController,
                    onSaved: (String val) {
                      _pass = val;
                    },
                  ),
                  new Container(
                      margin: const EdgeInsets.only(top: 20.0),
                      child: RaisedButton(
                        padding: const EdgeInsets.all(8.0),
                        textColor: Colors.white,
                        color: Colors.blue,
                        onPressed: () {
                          doLogin();
                        },
                        child: new Text("Login"),
                      )),
                  new Container(
                      child: RaisedButton(
                    padding: const EdgeInsets.all(4.0),
                    textColor: Colors.black,
                    color: Colors.white,
                    onPressed: () {
                      Router.navigateTo(context, RegisterScreen());
                    },
                    child: new Text("Register"),
                  ))
                ],
              ),
            ),
          )
        ]));
  }

  void doLogin() {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      print("Email: ${_email.toString()}  Pass: ${_pass.toString()}");
      _con.login(_email, _pass);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  void navigateToProfileIfNeed() {
    print("navigateToProfile token ${_con.authToken}");
    if (_con.authToken.isNotEmpty) {
      Router.replace(context, ProfileScreen());
    }
  }
}

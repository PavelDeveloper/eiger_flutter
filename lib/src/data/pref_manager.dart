import 'package:shared_preferences/shared_preferences.dart';

class PrefManager {
  
  static saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

 static Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    print('Token pref $token');
    return token;
  }
}

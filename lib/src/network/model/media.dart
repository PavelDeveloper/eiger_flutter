import 'package:eiger/src/network/model/created_by.dart';

class Media {
  int id;
  int eventId;
  String originalName;
  String extension;
  String size;
  String url;
  String urlThumbnail;
  String createdAt;
  String updatedAt;
  int createdAtTimestamp;
  int updatedAtTimestamp;
  CreatedBy createdBy;

  Media(
      {this.id,
        this.eventId,
        this.originalName,
        this.extension,
        this.size,
        this.url,
        this.urlThumbnail,
        this.createdAt,
        this.updatedAt,
        this.createdAtTimestamp,
        this.updatedAtTimestamp,
        this.createdBy});

  Media.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    eventId = json['event_id'];
    originalName = json['original_name'];
    extension = json['extension'];
    size = json['size'];
    url = json['url'];
    urlThumbnail = json['url_thumbnail'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdAtTimestamp = json['created_at_timestamp'];
    updatedAtTimestamp = json['updated_at_timestamp'];
    createdBy = json['created_by'] != null
        ? new CreatedBy.fromJson(json['created_by'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['event_id'] = this.eventId;
    data['original_name'] = this.originalName;
    data['extension'] = this.extension;
    data['size'] = this.size;
    data['url'] = this.url;
    data['url_thumbnail'] = this.urlThumbnail;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_at_timestamp'] = this.createdAtTimestamp;
    data['updated_at_timestamp'] = this.updatedAtTimestamp;
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    return data;
  }
}
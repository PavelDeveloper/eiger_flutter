import 'package:eiger/src/network/model/event_data.dart';

class EventsResponse {
  EventData data;

  EventsResponse({this.data});

  EventsResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new EventData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

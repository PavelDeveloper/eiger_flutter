import 'package:eiger/src/network/model/ended_at.dart';
import 'package:eiger/src/network/model/created_by.dart';
import 'package:eiger/src/network/model/user.dart';
import 'package:eiger/src/network/model/media.dart';



class MyEvents {
  int id;
  String title;
  String slug;
  String description;
  EndedAt endedAt;
  bool isPrivate;
  bool isFavorite;
  bool withPassword;
  List<Media> media;
  String createdAt;
  String updatedAt;
  int createdAtTimestamp;
  int updatedAtTimestamp;
  CreatedBy createdBy;
  List<User> users;
  String privateKey;
  bool ableToInvite;

  MyEvents(
      {this.id,
      this.title,
      this.slug,
      this.description,
      this.endedAt,
      this.isPrivate,
      this.isFavorite,
      this.withPassword,
      this.media,
      this.createdAt,
      this.updatedAt,
      this.createdAtTimestamp,
      this.updatedAtTimestamp,
      this.createdBy,
      this.users,
      this.privateKey,
      this.ableToInvite});

  MyEvents.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    slug = json['slug'];
    description = json['description'];
    endedAt = json['ended_at'] != null
        ? new EndedAt.fromJson(json['ended_at'])
        : null;
    isPrivate = json['is_private'];
    isFavorite = json['is_favorite'];
    withPassword = json['with_password'];
    if (json['media'] != null) {
      media = new List<Media>();
      json['media'].forEach((v) {
        media.add(new Media.fromJson(v));
      });
    }
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdAtTimestamp = json['created_at_timestamp'];
    updatedAtTimestamp = json['updated_at_timestamp'];
    createdBy = json['created_by'] != null
        ? new CreatedBy.fromJson(json['created_by'])
        : null;
    if (json['users'] != null) {
      users = new List<User>();
      json['users'].forEach((v) {
        users.add(new User.fromJson(v));
      });
    }
    privateKey = json['private_key'];
    ableToInvite = json['able_to_invite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['slug'] = this.slug;
    data['description'] = this.description;
    if (this.endedAt != null) {
      data['ended_at'] = this.endedAt.toJson();
    }
    data['is_private'] = this.isPrivate;
    data['is_favorite'] = this.isFavorite;
    data['with_password'] = this.withPassword;
    if (this.media != null) {
      data['media'] = this.media.map((v) => v.toJson()).toList();
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_at_timestamp'] = this.createdAtTimestamp;
    data['updated_at_timestamp'] = this.updatedAtTimestamp;
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.users != null) {
      data['users'] = this.users.map((v) => v.toJson()).toList();
    }
    data['private_key'] = this.privateKey;
    data['able_to_invite'] = this.ableToInvite;
    return data;
  }
}

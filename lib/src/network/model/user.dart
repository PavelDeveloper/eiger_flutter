import 'package:eiger/src/network/model/created_at.dart';
import 'package:eiger/src/network/model/updated_at.dart';

class User {
  int id;
  String name;
  String email;
  String phone;
  String birthday;
  bool ableToInvite;
  int invitedBy;
  String avatarUrl;
  CreatedAt createdAt;
  UpdatedAt updatedAt;

  int fiendshipId;
  User(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.birthday,
        this.ableToInvite,
        this.invitedBy,
        this.avatarUrl,
        this.createdAt,
        this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthday = json['birthday'];
    ableToInvite = json['able_to_invite'];
    invitedBy = json['invited_by'];
    avatarUrl = json['avatar_url'];
    createdAt = json['created_at'] != null
        ? new CreatedAt.fromJson(json['created_at'])
        : null;
    updatedAt = json['updated_at'] != null
        ? new UpdatedAt.fromJson(json['updated_at'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['birthday'] = this.birthday;
    data['able_to_invite'] = this.ableToInvite;
    data['invited_by'] = this.invitedBy;
    data['avatar_url'] = this.avatarUrl;
    if (this.createdAt != null) {
      data['created_at'] = this.createdAt.toJson();
    }
    if (this.updatedAt != null) {
      data['updated_at'] = this.updatedAt.toJson();
    }
    return data;
  }
}
import 'package:eiger/src/network/model/my_events.dart';

class EventData {
  List<MyEvents> myEvents;
  List<MyEvents> friendsEvents;
  List<MyEvents> favoriteEvents;

  EventData({this.myEvents, this.friendsEvents, this.favoriteEvents});

  EventData.fromJson(Map<String, dynamic> json) {
    if (json['my_events'] != null) {
      myEvents = new List<MyEvents>();
      json['my_events'].forEach((v) {
        myEvents.add(new MyEvents.fromJson(v));
      });
    }
    if (json['friends_events'] != null) {
      friendsEvents = new List<MyEvents>();
      json['friends_events'].forEach((v) {
        friendsEvents.add(new MyEvents.fromJson(v));
      });
    }
    if (json['favorite_events'] != null) {
      favoriteEvents = new List<MyEvents>();
      json['favorite_events'].forEach((v) {
        favoriteEvents.add(new MyEvents.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.myEvents != null) {
      data['my_events'] = this.myEvents.map((v) => v.toJson()).toList();
    }
    if (this.friendsEvents != null) {
      data['friends_events'] =
          this.friendsEvents.map((v) => v.toJson()).toList();
    }
    if (this.favoriteEvents != null) {
      data['favorite_events'] =
          this.favoriteEvents.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

import 'package:eiger/src/network/model/my_events.dart';

class EventResponse{
  MyEvents data;
  String exception;

  EventResponse({this.data});

  EventResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new MyEvents.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }


}

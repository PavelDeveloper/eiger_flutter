import 'package:eiger/src/network/model/friendship_data.dart';

class FriendshipResponse {
  FriendshipData data;

  FriendshipResponse({this.data});

  FriendshipResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new FriendshipData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}
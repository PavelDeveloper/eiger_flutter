import 'package:eiger/src/network/model/pending.dart';
import 'package:eiger/src/network/model/accepted.dart';

class FriendshipData {
  Pending pending;
  List<Accepted> accepted;
 

  FriendshipData({this.pending, this.accepted});

  FriendshipData.fromJson(Map<String, dynamic> json) {
    pending =
        json['pending'] != null ? new Pending.fromJson(json['pending']) : null;
    if (json['accepted'] != null) {
      accepted = new List<Accepted>();
      json['accepted'].forEach((v) {
        accepted.add(new Accepted.fromJson(v));
      });
    }
    }
  
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pending != null) {
      data['pending'] = this.pending.toJson();
    }
    if (this.accepted != null) {
      data['accepted'] = this.accepted.map((v) => v.toJson()).toList();
    }
    return data;
  }
  }
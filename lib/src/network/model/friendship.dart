import 'package:eiger/src/network/model/user.dart';

class Frindship {
  int id;
  User sender;
  User recipient;
  String createdAt;
  String updatedAt;

  Frindship({this.id, this.sender, this.recipient, this.createdAt, this.updatedAt});

  Frindship.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sender =
        json['sender'] != null ? new User.fromJson(json['sender']) : null;
    recipient = json['recipient'] != null
        ? new User.fromJson(json['recipient'])
        : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.sender != null) {
      data['sender'] = this.sender.toJson();
    }
    if (this.recipient != null) {
      data['recipient'] = this.recipient.toJson();
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
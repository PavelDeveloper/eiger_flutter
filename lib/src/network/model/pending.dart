import 'package:eiger/src/network/model/friendship.dart';

class Pending {
  List<Frindship> my;
  List<Frindship> toMe;

  Pending({this.my, this.toMe});

  Pending.fromJson(Map<String, dynamic> json) {
    if (json['my'] != null) {
      my = new List<Frindship>();
      json['my'].forEach((v) {
        my.add(new Frindship.fromJson(v));
      });
    }
    if (json['to_me'] != null) {
      toMe = new List<Frindship>();
      json['to_me'].forEach((v) {
        toMe.add(new Frindship.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.my != null) {
      data['my'] = this.my.map((v) => v.toJson()).toList();
    }
    if (this.toMe != null) {
      data['to_me'] = this.toMe.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

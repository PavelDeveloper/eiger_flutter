import 'dart:convert';

import 'package:eiger/constants.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:http/http.dart';

class ApiClient {
  String token;
  final Client _client = Client();

  Future<Response> get(String path, {Map<String, String> headers}) async {
    await getToken();
    return await _client
        .get(Uri.parse("${Constants.BASE_URL}$path"), headers: getTokenHeader())
        .then((result) {
      customInterceptor(result);
      return result;
    });
  }

  Future<Response> post(String path,
      {Map body, Map<String, String> headers}) async {
    await getToken();
    return await _client
        .post("${Constants.BASE_URL}$path",
            body: utf8.encode(json.encode(body)), headers: getTokenHeader())
        .then((result) {
      customInterceptor(result, body: body);
      return result;
    });
  }

  Future<Response> patch(String path,
      {Map body, Map<String, String> headers}) async {
    await getToken();
    return await _client
        .patch("${Constants.BASE_URL}$path",
            body: utf8.encode(json.encode(body)), headers: getTokenHeader())
        .then((result) {
      customInterceptor(result, body: body);
      return result;
    });
  }

  Future<Response> put(String path, Map body,
      {Map<String, String> headers}) async {
    await getToken();
    return await _client
        .put("${Constants.BASE_URL}$path",
            body: utf8.encode(json.encode(body)), headers: getTokenHeader())
        .then((result) {
      customInterceptor(result, body: body);
      return result;
    });
  }

  Future<Response> delete(String path, {Map<String, String> headers}) async {
    await getToken();
    return await _client
        .delete("${Constants.BASE_URL}$path", headers: getTokenHeader())
        .then((result) {
      customInterceptor(result);
      return result;
    });
  }

  void customInterceptor(Response response, {Map body}) {
    print("${response.request.toString()}");
    print("Response: ${json.encode(body)}");
    print(
        "Headers: ${response.headers}\n Status: ${response.statusCode} Body: ${response.body}");
  }

  getToken() async {
    await PrefManager.getToken().then((token) {
      this.token = token;
    });
  }

  Map<String, String> getTokenHeader() {
    Map<String, String> map = {'content-type': 'application/json'};
    if (token != null && token.isNotEmpty) {
      map.addAll({'Authorization': "Bearer ${token}"});
    }
    print("TOKEN HEADERS ${map}");
    return map;
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:eiger/constants.dart';
import 'package:eiger/src/network/api_client.dart';
import 'package:eiger/src/network/model/auth_response.dart';
import 'package:eiger/src/network/model/event.dart';
import 'package:eiger/src/network/model/events_response.dart';
import 'package:eiger/src/network/model/friends_response.dart';
import 'package:eiger/src/network/model/friendship_response.dart';
import 'package:eiger/src/network/model/profile_response.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class ApiManager {
  String token;
  ApiClient client = ApiClient();

  Future<EventResponse> createEvent(String title) async {
    Map map = {"title": title};
    return await client
        .post("events/", body: map)
        .then((resp) => EventResponse.fromJson(json.decode(resp.body)));
  }

  Future<EventResponse> createPublicEvent(
      String title, String description, bool isPrivate) async {
    Map map = {"title": title};
    if (description != null) {
      map.addAll({"description": description});
    }
    if (isPrivate != null) {
      map.addAll({"is_private": isPrivate});
    }
    return await client
        .post("events/", body: map)
        .then((resp) => EventResponse.fromJson(json.decode(resp.body)));
  }

  Future<AuthResponse> login(String email, String password) async {
    return await client.post("auth/login", body: {
      "email": email,
      "password": password
    }).then((response) => AuthResponse.fromJson(json.decode(response.body)));
  }

  Future<AuthResponse> register(String email, String name, String password,
      String passwordConfirm) async {
    return await client.post("auth/register", body: {
      "email": email,
      "name": name,
      "password": password,
      "password_confirmation": passwordConfirm
    }).then((response) => AuthResponse.fromJson(json.decode(response.body)));
  }

  Future<EventResponse> getPublicEvent(String slug) async {
    return await client
        .get("events/$slug")
        .then((response) => EventResponse.fromJson(json.decode(response.body)));
  }

  Future<EventResponse> deleteEvent(String slug) async {
    return await client
        .delete("events/$slug")
        .then((response) => EventResponse.fromJson(json.decode(response.body)));
  }


  Future<ProfileResponse> getProfile() async {
    return await client.get("auth/profile").then(
        (response) => ProfileResponse.fromJson(json.decode(response.body)));
  }

  Future<EventResponse> getPrivateEvent(String slug, String privateKey) async {
    return await client
        .get("events/$slug?${Constants.PRIVATE_KEY}$privateKey")
        .then((response) => EventResponse.fromJson(json.decode(response.body)));
  }

  Future<EventsResponse> getAllEvents() async {
    return await client.get("events").then(
        (response) => EventsResponse.fromJson(json.decode(response.body)));
  }

  Future<FriendsResponse> getFriends() async {
    return await client.get("friends").then(
        (response) => FriendsResponse.fromJson(json.decode(response.body)));
  }

  Future<FriendsResponse> getUsers(String query) async {
    return await client.get("users?query=$query").then(
        (response) => FriendsResponse.fromJson(json.decode(response.body)));
  }

  Future<FriendshipResponse> getFriendships() async {
    return await client.get("friendships").then(
        (response) => FriendshipResponse.fromJson(json.decode(response.body)));
  }

  Future<bool> deleteFriendship(String id) async {
    return await client.delete("friendships/$id").then((response) {
      if (response.statusCode >= 200 && response.statusCode <= 299) {
        return true;
      } else {
        return false;
      }
    });
  }

  Future<bool> deleteFriend(String email) async {
    return await client.delete("friends/$email").then((response) {
      if (response.statusCode >= 200 && response.statusCode <= 299) {
        return true;
      } else {
        return false;
      }
    });
  }

  Future<bool> acceptFriend(String id) async {
    return await client
        .patch("friendships/$id/accept-request")
        .then((response) {
      if (response.statusCode >= 200 && response.statusCode <= 299) {
        return true;
      } else {
        return false;
      }
    });
  }

  Future<bool> addToFriends(String email) async {
    return await client
        .post("friendships/$email/send-request")
        .then((response) {
      if (response.statusCode >= 200 && response.statusCode <= 299) {
        return true;
      } else {
        return false;
      }
    });
  }

  Future<http.StreamedResponse> uploadPhoto(
      String slug, File fileOriginal, File fileCroped) async {
    var postUri = Uri.parse(Constants.BASE_URL+"events/${slug.toString()}/media");
    var request = new http.MultipartRequest("POST", postUri);

    final orgFileName = fileOriginal.path.substring(
        fileOriginal.path.lastIndexOf("/") + 1, fileOriginal.path.length);

    final crpFileName = fileCroped.path.substring(
        fileCroped.path.lastIndexOf("/") + 1, fileCroped.path.length);

    request.files.add(new http.MultipartFile.fromBytes(
        'files[0][original]', await fileOriginal.readAsBytes(),
        filename: orgFileName, contentType: MediaType.parse("image/*")));

    request.files.add(new http.MultipartFile.fromBytes(
        'files[0][thumb]', await fileCroped.readAsBytes(),
        filename: crpFileName, contentType: MediaType.parse("image/*")));

    print(postUri);
    print("FILE ${fileOriginal.path}");
    return await request.send();
  }
}

import 'package:eiger/src/screens/gallery_screen.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/screens/event_screen.dart';
import 'package:eiger/src/screens/home_screen.dart';
import 'package:eiger/src/screens/login_screen.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:eiger/src/screens/register_screen.dart';
import 'package:eiger/src/screens/search_screen.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class MVCApp extends AppMVC {
  MVCApp({Key key}) : super(key: key);

  // MVCApp():super(con: EventController());

  static MaterialApp _app;

  static String get title => 'Eiger';

  Widget build(BuildContext context) {
    _app = MaterialApp(title: 'Eiger', theme: ThemeData.light(), routes: {
      HomeScreen.ROUTE: (context) => HomeScreen(),
      EventScreen.ROUTE: (context) => EventScreen(data: null),
      LoginScreen.ROUTE: (context) => LoginScreen(),
      RegisterScreen.ROUTE: (context) => RegisterScreen(),
      ProfileScreen.ROUTE: (context) => ProfileScreen(),
      SearchScreen.ROUTE: (context) => SearchScreen(),
      GalleryScreen.ROUTE: (context) => GalleryScreen(media: null)
    });
    return _app;
  }
}

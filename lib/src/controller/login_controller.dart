import 'package:eiger/src/common/router.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/app.dart';
import 'package:eiger/src/model/login_model.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class LoginController extends ControllerMVC {
  LoginController() {
    con = this;
  }

  String authToken = "";
  String errorMessage = "";

  static LoginController con;

  static LoginModel model = new LoginModel();

  @override
  void initState() {}

  String get title => MVCApp.title;

  void login(String email, String pass) async {
    await model.login(email, pass).then((response) {
     // setState(() {
      if(response.accessToken != null) {
        print("token ${response.accessToken}");
        authToken = response.accessToken;
        PrefManager.saveToken(authToken);
        Router.replaceAllScreens(context, ProfileScreen.ROUTE);
      } else {
        errorMessage = response?.message;
      }
      setState((){});
      });
  //  });
  }
}

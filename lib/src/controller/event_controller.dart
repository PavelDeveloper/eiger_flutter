import 'dart:io';

import 'package:eiger/src/app.dart' show MVCApp;
import 'package:eiger/src/common/router.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:eiger/src/model/event_model.dart' show EventModel;
import 'package:eiger/src/network/model/media.dart';
import 'package:eiger/src/network/model/my_events.dart';
import 'package:mvc_pattern/mvc_pattern.dart' show ControllerMVC;

class EventController extends ControllerMVC {
  MyEvents data;

  String token;

  EventController() {
    con = this;
  }

  static EventController con;

  static EventModel model = new EventModel();

  @override
  void initState() {}

  void initData(MyEvents data) {
    if (this.data == null) {
      this.data = data;
      getToken();
    }
  }

  String get title => MVCApp.title;

  void getPublicEvent() async {
    await model.getPublicEvent(data.slug).then((response) {
      setState(() {
        data = response.data;
      });
    });
  }

  void uploadPhoto(String slug, File fileOriginal, File fileCroped) async {
    model.uploadPhoto(slug, fileOriginal, fileCroped).then((response) {
      print("UPLOAD FILE: ${response.statusCode} ${response.toString()}");
      if (response.statusCode >= 200 && response.statusCode < 300) {
        getPublicEvent();
      } else {}
    });
  }

  String getDescription() {
    if (data.description == null) {
      return "";
    } else {
      return data.description;
    }
  }

  List<Media> getMedia() {
    if (data.media == null) {
      return new List(0);
    } else {
      return data.media;
    }
  }

  void deleteEvent() async {
    await model.deleteEvent(data.slug).then((response) {
      Router.pop(context);
    });
  }

  void getToken() {
    PrefManager.getToken().then((token) {
      setState(() {
        this.token = token;
      });
      setState((){});
    });
  }
}

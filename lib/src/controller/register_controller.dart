
import 'package:eiger/src/common/router.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/app.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:eiger/src/model/register_model.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:mvc_pattern/mvc_pattern.dart';


class RegisterController extends ControllerMVC {
  RegisterController() {
  con = this;
}

String authToken = "";

static RegisterController con;

static RegisterModel model = new RegisterModel();

@override
void initState() {}

String get title => MVCApp.title;

void register(String email, String name, String pass, String passConf) async {
  await model.register(email, name, pass, passConf).then((response) {
    // setState(() {
    print("token ${response.accessToken}");
    authToken = response.accessToken;
    PrefManager.saveToken(authToken);
    Router.replaceAllScreens(context, ProfileScreen.ROUTE);
  });
}
}

import 'package:eiger/src/app.dart';
import 'package:eiger/src/common/router.dart';
import 'package:eiger/src/model/profile_model.dart';
import 'package:eiger/src/network/model/event_data.dart';
import 'package:eiger/src/network/model/profile_data.dart';
import 'package:eiger/src/screens/event_screen.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ProfileController extends ControllerMVC {
  ProfileController() {
    con = this;
  }

  bool isLoading = true;
  ProfileData data;
  EventData eventData;
  static ProfileController con;

  static ProfileModel model = new ProfileModel();

  @override
  void initState() {}

  String get title => MVCApp.title;

  void getProfile() async {
    isLoading = true;
    await model.getProfile().then((response) {
      data = response.data;
      getAllEvents();
    }).catchError((error) {
      print("Error ${error.toString()}");
      setState(() {
        isLoading = false;
      });
    });
  }

  void getAllEvents() async {
    await model.getEvents().then((response) {
      setState(() {
        eventData = response.data;
        isLoading = false;
      });
    }).catchError((error) {
      print("Error ${error.toString()}");
      setState(() {
        isLoading = false;
      });
    });
  }

  void createEvent(String title, String description, bool isPrivate) async {
    await model.createEvent(title, description, isPrivate).then((response) {
      isLoading = false;
      Navigator.pop(context);
      Router.navigateTo(context, EventScreen(data: response.data));
    });
  }

  String getAvatar() {
    if (data == null || data.avatarUrl == null || data.avatarUrl.isEmpty) {
      return "";
    } else {
      return data.avatarUrl;
    }
  }

  String getName() {
    if (data == null || data.name == null) {
      return "";
    } else {
      return data.name;
    }
  }

  String getSwitchState(bool isActive) {
    if (isActive) {
      return "Private Event";
    } else {
      return "Public Event";
    }
  }
}

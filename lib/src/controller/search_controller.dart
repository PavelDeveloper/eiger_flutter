import 'package:eiger/src/app.dart';
import 'package:eiger/src/model/search_model.dart';
import 'package:eiger/src/network/model/user.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SearchController extends ControllerMVC {
  List<User> friends;

  List<User> search;

  List<User> toMe;

  List<User> my;

  SearchController() {
    con = this;
    friends = new List<User>();
    search = new List<User>();
    toMe = new List<User>();
    my = new List<User>();
  }

  String authToken = "";

  static SearchController con;

  static SearchModel model = new SearchModel();

  @override
  void initState() {
    getFriends();
    getFriendship();
  }

  String get title => MVCApp.title;

  void getFriends() async {
    await model.getFriends().then((response) {
      setState(() {
        friends = response.data;
      });
    });
  }

  void getFriendship() {
    model.getFriendship().then((response) {
      List<User> toMeList = new List();
      for (var fr in response.data.pending.toMe) {
        User user = fr.sender;
        user.fiendshipId = fr.id;
        toMeList.add(user);
      }
      toMe = toMeList;

      List<User> myList = new List();
      for (var fr in response.data.pending.my) {
        User user = fr.recipient;
        user.fiendshipId = fr.id;
        myList.add(user);
      }
      setState(() {
        my = myList;
      });
    });
  }

  void getUsers(String query) {
    model.getUsers(query).then((response) {
      setState(() {
        search = response.data;
      });
    });
  }

  void deleteFriend(User user) {
    model.deleteFriend(user.email).then((success) {
      if (success) {
        setState(() {
          friends.remove(user);
        });
      }
    });
  }

  void acceptFriend(User user) {
    model.acceptFriend(user.fiendshipId.toString()).then((success) {
      if (success) {
        setState(() {
          friends.add(user);
          toMe.remove(user);
        });
      }
    });
  }

  void addToFriends(User user) {
    model.addToFriends(user.email).then((success) {
      if (success) {
        setState(() {
          my.add(user);
        });
      }
    });
  }

  void deleteFriendship(User user, int type) {
    model.deleteFriendship(user.fiendshipId.toString()).then((success) {
      if (success) {
        setState(() {
          if (type == 2) {
            my.remove(user);
          }

          if (type == 3) {
            toMe.remove(user);
          }
        });
      }
    });
  }
}

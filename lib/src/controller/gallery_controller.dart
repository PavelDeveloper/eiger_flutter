import 'package:eiger/src/common/router.dart';
import 'package:flutter/material.dart';
import 'package:eiger/src/app.dart';
import 'package:eiger/src/model/login_model.dart';
import 'package:eiger/src/screens/profile_screen.dart';
import 'package:eiger/src/data/pref_manager.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class GalleryController extends ControllerMVC {
  GalleryController() {
    con = this;
  }

  String authToken = "";

  static GalleryController con;

  static LoginModel model = new LoginModel();

  @override
  void initState() {}

  String get title => MVCApp.title;
}

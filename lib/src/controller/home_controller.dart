import 'package:eiger/src/app.dart' show MVCApp;
import 'package:eiger/src/model/home_model.dart' show Model;
import 'package:eiger/src/network/model/event.dart';
import 'package:mvc_pattern/mvc_pattern.dart' show ControllerMVC;

class Con extends ControllerMVC {
  factory Con() {
    if (_this == null) _this = Con._();
    return _this;
  }

  static Con _this;

  Con._();

  static Con get con => _this;

  static final model = Model();

  String get title => MVCApp.title;

  Future<EventResponse> createEvent(String text) {
    return model.createEvent(text);
  }

  Future<EventResponse> getPublicEvent(String slug) {
    return model.getPublicEvent(slug);
  }
}

import 'package:eiger/src/network/api_manager.dart' as apiManger;
import 'package:eiger/src/network/model/auth_response.dart';

class RegisterModel {

  RegisterModel() {
    model = this;
  }

  static RegisterModel model;

  var api = apiManger.ApiManager();

  Future<AuthResponse> register(
      String email, String name, String pass, String passConfirm) {
    return api.register(email, name, pass, passConfirm);
  }
}

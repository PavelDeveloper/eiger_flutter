import 'package:eiger/src/network/api_manager.dart' as apiManger;
import 'package:eiger/src/network/model/profile_response.dart';
import 'package:eiger/src/network/model/events_response.dart';
import 'package:eiger/src/network/model/event.dart';

class ProfileModel {
  ProfileModel() {
    model = this;
  }

  static ProfileModel model;

  var api = apiManger.ApiManager();

  Future<ProfileResponse> getProfile() {
    return api.getProfile();
  }


 Future<EventsResponse> getEvents() {
    return api.getAllEvents();
  }


 Future<EventResponse> createEvent(String title, String desc, bool isPrivate) {
    return api.createPublicEvent(title, desc, isPrivate);
  }
}
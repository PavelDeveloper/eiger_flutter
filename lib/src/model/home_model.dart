import 'package:eiger/src/network/model/event.dart';
import 'package:eiger/src/network/api_manager.dart' as apiManger;


class Model {
  var api = apiManger.ApiManager();

  Future<EventResponse> createEvent(String text) {
    return api.createPublicEvent(text, null, null);
  }

  Future<EventResponse> getPublicEvent(String slug) {
    return api.getPublicEvent(slug);
  }

  Future<EventResponse> getPrivateEvent(String slug, String prvateKey) {
    return api.getPrivateEvent(slug, prvateKey);
  }
}

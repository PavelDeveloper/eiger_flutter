import 'dart:io';

import 'package:http/http.dart';
import 'package:eiger/src/network/model/event.dart';
import 'package:eiger/src/network/api_manager.dart' as apiManger;

class EventModel {
  EventModel() {
    model = this;
  }

  static EventModel model;

  var api = apiManger.ApiManager();

  Future<EventResponse> getPublicEvent(String slug) {
    return api.getPublicEvent(slug);
  }

  Future<EventResponse> getPrivateEvent(String slug, String prvateKey) {
    return api.getPrivateEvent(slug, prvateKey);
  }

  Future<StreamedResponse> uploadPhoto(String slug, File fileOriginal, File fileCroped) {
    return api.uploadPhoto(slug, fileOriginal, fileCroped);
  }

  Future<EventResponse> deleteEvent(String slug) {
    return api.deleteEvent(slug);
  }
}

import 'package:eiger/src/network/api_manager.dart' as apiManger;
import 'package:eiger/src/network/model/auth_response.dart';

class LoginModel {
  LoginModel() {
    model = this;
  }

  static LoginModel model;

  var api = apiManger.ApiManager();

  Future<AuthResponse> login(String email, String pass) {
    return api.login(email, pass);
  }

  Future<AuthResponse> register(
      String email, String name, String pass, String passConfirm) {
    return api.register(email, name, pass, passConfirm);
  }


}

import 'package:eiger/src/network/api_manager.dart' as apiManger;
import 'package:eiger/src/network/model/friends_response.dart';
import 'package:eiger/src/network/model/friendship_response.dart';

class SearchModel {
  SearchModel() {
    model = this;
  }

  static SearchModel model;

  var api = apiManger.ApiManager();

  Future<FriendsResponse> getUsers(String query) {
    return api.getUsers(query);
  }

  Future<FriendsResponse> getFriends() {
    return api.getFriends();
  }

  Future<FriendshipResponse> getFriendship() {
    return api.getFriendships();
  }

  Future<bool> deleteFriend(String email) {
    return api.deleteFriend(email);
  }

  Future<bool> acceptFriend(String id) {
    return api.acceptFriend(id);
  }


  Future<bool> addToFriends(String email) {
    return api.addToFriends(email);
  }

  Future<bool> deleteFriendship(String id) {
    return api.deleteFriendship(id);
  }
}
